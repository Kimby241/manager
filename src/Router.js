import React from 'react';
import { Scene, Router, Actions, ActionConst } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import SignUp from './components/SignUp';
import RestauList from './components/RestauList';
import Home from './components/Home';
import RestauSearch from './components/RestauSearch';
import NavigationDrawer from './components/NavigationDrawer';
import { Text, View, Image, Icon, TouchableOpacity } from 'react-native';
import { 
	TXT_TITLE_LOGIN, 
	TXT_TITLE_RESTAU_LIST,
	TXT_SEARCH_RESTAU_LABEL
} from './actions/texts';

const RouterComponent = () => {
	return (
		<Router >
				<Scene key="auth" initial >
					<Scene key="login" component={ LoginForm } hideNavBar={true}/>
				</Scene>
				<Scene key="main" >
					<Scene 
						onRight={ () => Actions.restauSearch() }
						rightTitle="Rechercher"
						key="restaulist" 
						component={ RestauList } 
						title={ TXT_TITLE_RESTAU_LIST } 
						hideNavBar={true}
					/>
					<Scene 
						key="restauSearch" 
						component={ RestauSearch } 
						title={ TXT_SEARCH_RESTAU_LABEL } 
						hideNavBar={false}
						
					/>
				</Scene>
		</Router>
	);
};

export default RouterComponent;