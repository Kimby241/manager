import React, { Component } from 'react';
// Redux
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import ReduxThunk from 'redux-thunk';
// firebase
import firebase from 'firebase';
// Components
import Router from './Router';

class App extends Component { 
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			name: '',
			password: '',
			loading: false,
			error: '',
			user: null
		}
		this.store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

		if (this.state.user) {
			if (this.state.user.token) {
				axios.defaults.headers.common['Authorisation'] = this.state.user.token ;
			} else {
				delete axios.defaults.headers.common['Authorisation'];
			}
		}
	}

	render() {
		console.log(this.state);
		console.log(this.store);
		return(
			<Provider store={this.store}>
				<Router/>
			</Provider> 
		); 
	} 
}

export default App;