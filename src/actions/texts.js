export const MSG_LOGIN_USER_FAIL = 'Login/Mot de passe incorrect';

export const TXT_EMAIL_LABEL = 'Email';
export const TXT_PASSWORD_LABEL = 'Mot de passe';
export const TXT_NAME_LABEL = 'Prénom';
export const TXT_TITLE_LOGIN = 'Connexion';
export const TXT_TITLE_RESTAU_LIST = 'HalalAuRestau';
export const TXT_SEARCH_RESTAU_LABEL = 'Recherche';
export const TXT_CONFIRM_PASSWORD_LABEL = 'Confirmer';
export const TXT_SIGN_UP_LABEL = 'Je m\'inscris';
export const MSG_SIGN_UP_USER_FAIL = 'Une erreur s\'est produite';

