import { 
	RATE_UPDATED,
	RATED_SUBMITED
} from './types';

export const rate_updated = ({ prop, value }) => {
	return { 
		type:  RATE_UPDATED,
		payload: { prop, value }
	};
};