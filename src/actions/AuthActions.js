import { Actions } from 'react-native-router-flux';
import firebase from 'firebase';
import axios from 'axios';

import { 
	EMAIL_CHANGED, 
	PASSWD_CHANGED,
	LOGIN_USER_SUCCESS,
	LOGIN_USER_FAIL,
	LOGIN_USER,
	LOADING_RESTAU_LIST_SUCCESS,
	LOADING_RESTAU_LIST_FAIL,
	SKIPED_LOGIN,
	NAME_CHANGED,
	SIGN_UP_USER,
	SIGN_UP_USER_SUCCESS,
	SIGN_UP_USER_FAIL
} from './types';

export const emailChanged = (text) => {
	return {
		type: EMAIL_CHANGED,
		payload: text
	};
};

export const nameChanged = (text) => {
	return {
		type: NAME_CHANGED,
		payload: text
	};
};

export const passwordChanged = (text) => {
	return {
		type: PASSWD_CHANGED,
		payload: text
	};
};

export const loginUser = ({email, password}) => {
	return (dispatch) => {
		dispatch ({type: LOGIN_USER});
		axios.post('https://www.halalauresto.fr/api/login_check', {
		    email: email,
		    password: password
		})
		.then(response => loginUserSuccess(dispatch, response, email))
		.catch(response => loginUserFail(dispatch, response));
	};
};

export const signUpUser = ({name, email, password}) => {
	console.log("Inscription");
	return (dispatch) => {
		dispatch ({type: SIGN_UP_USER});
		axios.post('https://www.halalauresto.fr/api/users', {
		    username: name,
		    email: email,
		    plainPassword: password
		})
		.then(response => signUpUserSuccess(dispatch, response))
		.catch(() => signUpUserFail(dispatch));
	};
};

const setAuthorisationToken = (token) => {
	if (token) {
		axios.defaults.headers.common['Authorization'] = token;
	} else {
		delete axios.defaults.headers.common['Authorisation'];
	}
}

const loginUserFail = (dispatch, response) => {
	console.log("Echec connexion");
	console.log(response);
	dispatch ({type: LOGIN_USER_FAIL});
}

const loginUserSuccess = (dispatch, user, email) => {
	console.log("Connexion OK");
	const token = user.data.token;
	console.log(user);
	const userdata = {
	    email: email,
	    token: token
	}

	dispatch({
		type: LOGIN_USER_SUCCESS,
		payload: userdata
	});	

	setAuthorisationToken(token);

	Actions.main();
	console.log("Done");
}

const signUpUserFail = (dispatch) => {
	console.log("Echec inscription");
	dispatch ({type: SIGN_UP_USER_FAIL});
}

const signUpUserSuccess = (dispatch, user) => {
	console.log("Inscription OK");
	console.log(user);
	dispatch({
		type: SIGN_UP_USER_SUCCESS,
		payload: user
	});
	console.log("Fin connexion");
	Actions.main();
}
