import { Actions } from 'react-native-router-flux';
import axios from 'axios';

import { 
	LOADING_RESTAU_LIST_SUCCESS,
	RESTAURANTS_FETCH_SUCCESS,
	LOADING_RESTAU_LIST_FAIL,
	SKIPED_LOGIN
} from './types';

export const restauListFetch = () => {
	console.log("Fetch reataurants");
	return (dispatch) => {
		axios.get('https://www.halalauresto.fr/api/restaurants/0/firstresults/32/maxresults')
		.then(response => {
			fetchingRestauListSucces(dispatch, response)
		})
		.catch((erreur) => {
				loadingRestauListFail(dispatch, erreur)
		});
	};
};

const fetchingRestauListSucces = (dispatch, response) => {
	console.log(response);
	dispatch ({type: RESTAURANTS_FETCH_SUCCESS, payload: response.data.restaurants});
}

export const loadingRestauList = () => {
	return (dispatch) => {
		axios.get('https://www.halalauresto.fr/api/restaurants/0/firstresults/32/maxresults')
		.then(response => {
			loadingRestauListSucces(dispatch, response)
		})
		.catch((erreur) => {
				loadingRestauListFail(dispatch, erreur)
				console.log(erreur);
		});
	};
};

const loadingRestauListSucces = (dispatch, response) => {
	console.log(response);
	dispatch ({type: LOADING_RESTAU_LIST_SUCCESS, payload: response.data.restaurants});
}

const loadingRestauListFail =(dispatch, error) => {
	dispatch ({type: LOADING_RESTAU_LIST_FAIL, payload: error});
}