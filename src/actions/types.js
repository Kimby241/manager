export const EMAIL_CHANGED = 'email_changed';
export const PASSWD_CHANGED = 'password_changed';
export const NAME_CHANGED = 'name_changed';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';
export const SIGN_UP_USER = 'signup_user';
export const LOADING_RESTAU_LIST_SUCCESS = 'loading_restau_list_success';
export const LOADING_RESTAU_LIST_FAIL = 'LOADING_RESTAU_LIST_FAIL';
export const SKIPED_LOGIN = 'SKIPED_LOGIN';
export const RATE_UPDATED = 'RATE_UPDATED';
export const RATED_SUBMITED = 'RATED_SUBMITED';
export const RESTAURANTS_FETCH_SUCCESS = 'RESTAURANTS_FETCH_SUCCESS';
export const SIGN_UP_USER_SUCCESS = 'SIGN_UP_USER_SUCCESS';
export const SIGN_UP_USER_FAIL = 'SIGN_UP_USER_FAIL';
