import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import RestauListReducer from './RestauListReducer';
import RestaurantReducer from './RestaurantReducer';

export default combineReducers({  
	auth: AuthReducer,
	restaulist: RestauListReducer,
	restaudetail: RestaurantReducer
});