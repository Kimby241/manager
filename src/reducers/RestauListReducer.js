import { 
	RESTAURANTS_FETCH_SUCCESS,
	LOADING_RESTAU_LIST_FAIL
} from '../actions/types';

import { 
	MSG_LOGIN_USER_FAIL
} from '../actions/texts';

const INITIAL_STATE =  { 
	restos: []
};

export default (state=INITIAL_STATE, action) => {
	switch(action.type)  {
		case RESTAURANTS_FETCH_SUCCESS:
			return {
				...state, restos: action.payload };
		case LOADING_RESTAU_LIST_FAIL:
			console.log(action.payload);
			return {
				...state, error: action.payload };
		default:
			return state;
	}
};
