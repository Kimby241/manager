import { 
	EMAIL_CHANGED, 
	PASSWD_CHANGED,
	NAME_CHANGED,
	LOGIN_USER_SUCCESS,
	LOGIN_USER_FAIL,
	LOGIN_USER,
	SKIPED_LOGIN,
	SIGN_UP_USER,
	SIGN_UP_USER_SUCCESS,
	SIGN_UP_USER_FAIL
} from '../actions/types';

import { 
	MSG_LOGIN_USER_FAIL,
	MSG_SIGN_UP_USER_FAIL
} from '../actions/texts';

const INITIAL_STATE =  { 
	name: '',
	email: '',
	password: '',
	user: null,
	error: '',
	loading: false
};

export default (state=INITIAL_STATE, action) => {
	//console.log(action);
	switch(action.type)  {
		case EMAIL_CHANGED:
			return { 
				...state, 
				email: action.payload };
		case NAME_CHANGED:
			return { 
				...state, 
				name: action.payload };
		case PASSWD_CHANGED:
			return { 
				...state, 
				password: action.payload };
		case LOGIN_USER_SUCCESS:
			return { 
				...state,
				...INITIAL_STATE,
				user: action.payload  };
		case LOGIN_USER_FAIL:
			return { 
				...state,
				error: MSG_LOGIN_USER_FAIL,
				password: '', 
				loading: false };
		case SIGN_UP_USER_SUCCESS : 
			return { 
				...state,
				...INITIAL_STATE,
				user: action.payload  };
		case SIGN_UP_USER_FAIL :
			return { 
				...state,
				error: MSG_SIGN_UP_USER_FAIL,
				password: '', 
				loading: false };
		case LOGIN_USER:
			return { 
				...state, error: '', 
				loading: true };
		case SIGN_UP_USER: //revoir
			return { 
				...state, error: '', 
				loading: true };
		default:
			return state;
	}
};