import { 
	LOADING_RESTAU_LIST_SUCCESS,
	LOADING_RESTAU_LIST_FAIL,
	RATE_UPDATED
} from '../actions/types';

import { 
	MSG_LOGIN_USER_FAIL
} from '../actions/texts';

const INITIAL_STATE =  { 
	comment: null,
	rate: 0,
	favoris: false
};

export default (state=INITIAL_STATE, action) => {
	//console.log(action);
	switch(action.type)  {
		case RATE_UPDATED:
			//action.payload === { prop: comment, value: 'Très bon' }
			return {
				...state, [action.payload.prop]: action.payload.value };
		default:
			return state;
	}
};
