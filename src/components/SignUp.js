import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Card, CardItem, cardBody, ListItem, Container, Text, Radio, Content, Form, Item, Input, Label, Button } from 'native-base';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, signUpUser, confirmpwchanged, nameChanged
} from '../actions';
import { CardSection, Spinner } from './common';
import { Actions } from 'react-native-router-flux';

import { 
	TXT_EMAIL_LABEL,
	TXT_PASSWORD_LABEL,
	TXT_CONFIRM_PASSWORD_LABEL,
	TXT_SIGN_UP_LABEL,
	TXT_NAME_LABEL
} from '../actions/texts';

class SignUpForm extends Component{ 

	onEmailChange (text) {
		this.props.emailChanged(text);
	}

	onPasswordChange (text) {
		this.props.passwordChanged(text);
	}

	onNameChange (text) {
		this.props.nameChanged(text);
	}

	onButtonPress() {
		const { email, password } = this.props;
		this.props.signUpUser({ email, password });
	}

	onSkip() {
		Actions.main();
	}

	renderButton() {
		if (this.props.loading) {
			return <Spinner size="large" /> ;
		}  

		return (
			<Button danger onPress={ this.onButtonPress.bind(this) } style={ styles.buttonStyle } block>
				<Text>{TXT_SIGN_UP_LABEL}</Text>
			</Button>
		);
	}
	
	render() {
		return(
			<Container style={styles.contentContainerStyle}>
				
				<Content style={styles.contentStyle}>
					<View style={styles.imageContainerStyle}>
						<Image 
							style={styles.ImageStyle}
							source={require('./logo_halalauresto_carre.jpg')}
						/>
					</View>
					<Form>
                        <Item underline>
	                        <Input 
	                        	placeholder={ TXT_NAME_LABEL }
	                        	onChangeText={ this.onNameChange.bind(this) }
	                        	value={ this.props.name }
	                        />
	                    </Item>
                        <Item underline>
	                        <Input 
	                        	placeholder={ TXT_EMAIL_LABEL }
	                        	onChangeText={ this.onEmailChange.bind(this) }
	                        	value={ this.props.email }
	                        />
	                    </Item>
	                    <Item underline>
	                        <Input placeholder={ TXT_PASSWORD_LABEL }
	                        secureTextEntry
	                        onChangeText={ this.onPasswordChange.bind(this) }
	                        value={ this.props.password }
	                         />
	                    </Item>
                    </Form>
					<Text style={ styles.errorTextStyle } >
						{ this.props.error }
					</Text>
					<View >
						{ this.renderButton() }
					</View>
				</Content>
			</Container>
		); 
	} 
}

const styles = {
	errorTextStyle: {
		fontSize: 20,
		alignSelf: 'center',
		color: 'red'
	},
	inputStyle: { 
		paddingRight: 5,
		paddingLeft: 5,
		flex: 2
	},
	containerStyle: { 
		height: 40,
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center'
	},
	contentContainerStyle: { 
		marginRight: 20,
		flex:1,
        justifyContent: 'center',
        alignSelf: 'stretch',
        flexDirection: 'column'
	},
	contentStyle: { 
		flex:1,
        alignSelf: 'stretch',
        flexDirection: 'column'
	},
	imageContainerStyle: { 
		flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        marginTop:20,
	},
	labelStyle: { 
		fontSize: 18,
		paddingLeft: 20,
		flex: 1 
	},
	ImageStyle: {
		height: 90,
		flex: 1,
		width: 150,
		resizeMode:'cover'
	},
	thumbnailContainerStyle: {
		marginLeft: 10,
		marginRight: 10
	},
	buttonStyle : {
		flex: 1,
		marginTop:20,
		marginLeft: 20,
		marginRight: 20
	}
}

const mapStateToProps = ({ auth }) => { 
	const { email, password, name, confirmpw, error, loading } = auth;
	return { email, password, name, confirmpw, error, loading };
};

export default connect(mapStateToProps, { 
	emailChanged, passwordChanged, confirmpwchanged, signUpUser, nameChanged
})(SignUpForm);