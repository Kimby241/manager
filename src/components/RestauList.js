import _ from 'lodash';
import React, { Component } from 'react';
import { ListView, View, Text, ScrollView } from 'react-native';
import axios from 'axios';
import RestauDetail from './RestauDetail';
import { restauListFetch } from '../actions';
import { connect } from 'react-redux';

class RestauList extends Component{
	componentWillMount() {
		this.props.restauListFetch();

		this.createDataSource(this.props);
	}

	componentWillReceiveProps(nextProps) {
		this.createDataSource(nextProps);
	}

	createDataSource({ restos }) {
		const ds = new ListView.DataSource({
			rowHasChanged: (r1, r2) => r1 !== r2
		});

		this.dataSource = ds.cloneWithRows(restos);
	}

	renderRestos() {
		return this.props.restos.map(resto =>  
			<RestauDetail key={resto.id} resto={resto} />);
	}

	renderRow(resto) {
		return <RestauDetail resto={resto} />
	}

	render() {
		return (
			<ListView
				enableEmptySections
				dataSource={this.dataSource}
				renderRow={this.renderRow}
			/>
		);
	}
}

const mapStateToProps = ({ restaulist }) => { 
	const restos = restaulist;

	return restos;
};

export default connect(mapStateToProps, { restauListFetch })(RestauList);