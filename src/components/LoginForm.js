import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, loginUser
} from '../actions';
import { Card, CardSection, Spinner } from './common';
import { Button, Input, Item, Label, Form, Container, Content } from 'native-base';
import { Actions } from 'react-native-router-flux';

import { 
	TXT_EMAIL_LABEL,
	TXT_PASSWORD_LABEL
} from '../actions/texts';

class LoginForm extends Component{ 

	onEmailChange (text) {
		this.props.emailChanged(text);
	}

	onPasswordChange (text) {
		this.props.passwordChanged(text);
	}

	onButtonPress() {
		const { email, password } = this.props;
		this.props.loginUser({ email, password });
	}

	onSkip() {
		console.log("skip");
		Actions.main();
	}

	renderButton() {
		if (this.props.loading) {
			return <Spinner size="large" /> ;
		}  

		return (
			<Button danger onPress={ this.onButtonPress.bind(this) } style={ styles.buttonStyle } block>
				<Text>Connexion</Text>
			</Button>
		);
	}

	renderSkipButton() {
		return (
			<Button danger onPress={ this.onSkip.bind(this) } style={ styles.buttonStyle } block>
				<Text>Bien plus tard</Text>
			</Button>
		);
	}
	
	render() {
		return(
			<Container style={styles.contentContainerStyle}>
				<Content style={styles.contentStyle}>
					<View style={styles.imageContainerStyle}>
						<Image 
							style={styles.ImageStyle}
							source={require('./logo_halalauresto_carre.jpg')}
						/>
					</View>
					<Form>
			        	<Item floatingLabel>
			            	<Label>Email</Label>
			            	<Input
								label={ TXT_EMAIL_LABEL }							
								onChangeText={ this.onEmailChange.bind(this) }
								value={ this.props.email }
							/>
			            </Item>
			            <Item floatingLabel>
			            	<Label>Mot de passe</Label>
			            	<Input 
								label={ TXT_PASSWORD_LABEL } 
								secureTextEntry
								onChangeText={ this.onPasswordChange.bind(this) }
								value={ this.props.password }
							/>
			            </Item>
			        </Form>

			        <Text style={ styles.errorTextStyle } >
						{ this.props.error }
					</Text>

					<View >
						{ this.renderButton() }
					</View>
					<View >
						{ this.renderSkipButton() }
					</View>	
				</Content>	
			</Container>
		); 
	} 
}

const styles = {
	errorTextStyle: {
		fontSize: 15,
		alignSelf: 'center',
		color: 'red'
	},
	buttonStyle : {
		flex: 1,
		marginTop:20,
		marginLeft: 20,
		marginRight: 20
	},
	contentContainerStyle: { 
		flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
	},
	contentStyle: { 
		marginRight: 20,
		flex:1,
        alignSelf: 'stretch',
        flexDirection: 'column'
	},
	ImageStyle: {
		height: 90,
		flex: 1,
		width: 150,
		resizeMode:'cover'
	},
	imageContainerStyle: { 
		flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        marginTop:20,
	}
}

const mapStateToProps = ({ auth }) => { 
	const { email, password, error, loading } = auth;
	return { email, password, error, loading };
};

export default connect(mapStateToProps, { 
	emailChanged, passwordChanged, loginUser
})(LoginForm);