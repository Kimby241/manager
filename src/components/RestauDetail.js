import React, { Component } from 'react';
import { Text, View, Image, TouchableWithoutFeedback } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Card, CardSection } from './common';

class RestauDetail extends Component{
	onRowPress() {
		console.log(this.props.resto.titreRestaurant);
		Actions.restauSearch();
	}

	render() {
		const {titreRestaurant, ville} = this.props.resto;
		const {
			thumbnailStyle, 
			headerContentStyle,
			thumbnailContainerStyle,
			headerTextStyle,
			ImageStyle
		} = styles;
		console.log("ligne ouverte");
		return (			
			<TouchableWithoutFeedback onPress={ this.onRowPress.bind(this)}>
				<View>
					<Card>
						<CardSection >
							<View style={thumbnailContainerStyle} >
								<Image 
									style={thumbnailStyle}
									source={require('./logo_halalauresto_carre.jpg')}
								/>
							</View>
							<View style={headerContentStyle}>
								<Text style={headerTextStyle}>{titreRestaurant}</Text>
								<Text>{ville}</Text>
							</View>
						</CardSection>
						<CardSection >
							<Image 
								style={ImageStyle}
								source={require('./logo_halalauresto_carre.jpg')}
							/>
						</CardSection>
						
					</Card>
				</View>
			</TouchableWithoutFeedback>
		);
	}
};

const styles = {
	headerContentStyle: {
		flexDirection: 'column',
		justifyContent: 'space-around'
	},
	thumbnailStyle:{
		height: 80,
		width: 80
	},
	thumbnailContainerStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		marginLeft: 10,
		marginRight: 10
	},
	headerTextStyle: {
		fontSize: 18
	},
	ImageStyle: {
		height: 300,
		flex: 1,
		width: null
	}
};

export default RestauDetail;
