import React from 'react';
import { Text, View, Image } from 'react-native';
import { Card, CardSection } from './common';

const RestauDetail = ({resto}) => {
	const {titreRestaurant, ville} = resto;
	const {
		thumbnailStyle, 
		headerContentStyle,
		thumbnailContainerStyle,
		headerTextStyle,
		ImageStyle
	} = styles;

	return (
		<Card>
			<CardSection >
				<View style={thumbnailContainerStyle} >
					<Image 
						style={thumbnailStyle}
						source={require('./logo_halalauresto_carre.jpg')}
					/>
				</View>
				<View style={headerContentStyle}>
					<Text style={headerTextStyle}>{titreRestaurant}</Text>
					<Text>{ville}</Text>
				</View>
			</CardSection>
			<CardSection >
				<Image 
					style={ImageStyle}
					source={require('./logo_halalauresto_carre.jpg')}
				/>
			</CardSection>
			
		</Card>
	);
};

const styles = {
	headerContentStyle: {
		flexDirection: 'column',
		justifyContent: 'space-around'
	},
	thumbnailStyle:{
		height: 80,
		width: 80
	},
	thumbnailContainerStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		marginLeft: 10,
		marginRight: 10
	},
	headerTextStyle: {
		fontSize: 18
	},
	ImageStyle: {
		height: 300,
		flex: 1,
		width: null
	}
};

export default RestauDetail;
