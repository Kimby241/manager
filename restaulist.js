import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import axios from 'axios';
import RestauDetail from './RestauDetail';

class RestauList extends Component{
	state = { restos: [] };
	componentWillMount() {
		axios.get('http://www.halalauresto.fr/api/restaurants/0/firstresults/12/maxresults')
			.then(response => this.setState(
				{	restos: response.data.restaurants,
					auth: {
					    username: 'halaloresto',
					    password: 'hlx2016'
					}
				}));
		axios.post('https://www.halalauresto.fr/api/login_check', {
		    firstName: 'Fred',
		    lastName: 'Flintstone'
		})
			.then(function (response) {
			console.log(response);
		})
		.catch(function (error) {
			console.log(error);
		});
	}
	
	renderRestos() {
		return this.state.restos.map(resto =>  
			<RestauDetail key={resto.id} resto={resto} />);
	}

	render() {
		console.log(this.state.restos);
		return (
			<ScrollView>
				{this.renderRestos()}
			</ScrollView>
		);
	}
}

export default RestauList;